
let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89,84,78,88],

    login() {
        console.log(`${this.email} has logged in`);
    },

    logout() {
        console.log(`${this.email} has logged out`);
    },

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
    },

    computeAve() {
        const gradeAverage = this.grades.reduce((accumulator, currentValue) => accumulator + currentValue, 0) / 4;
        return gradeAverage;
    },

    willPass() {
        const gradeAverage = this.computeAve();
        return gradeAverage >= 85;
    },

    willPassWithHonors() {
        const gradeAverage = this.computeAve();
        if(gradeAverage >= 90) {
            return true
        } else if(gradeAverage >= 85 && gradeAverage < 90) {
            return false
        } else {
            return undefined
        }
    }
}

let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],

    login() {
        console.log(`${this.email} has logged in`);
    },

    logout() {
        console.log(`${this.email} has logged out`);
    },

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
    },

    computeAve() {
        const gradeAverage = this.grades.reduce((accumulator, currentValue) => accumulator + currentValue, 0) / 4;
        return gradeAverage;
    },

    willPass() {
        const gradeAverage = this.computeAve();
        return gradeAverage >= 85;
    },

    willPassWithHonors() {
        const gradeAverage = this.computeAve();
        if(gradeAverage >= 90) {
            return true
        } else if(gradeAverage >= 85 && gradeAverage < 90) {
            return false
        } else {
            return undefined
        }
    }
}

let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    
    login() {
        console.log(`${this.email} has logged in`);
    },

    logout() {
        console.log(`${this.email} has logged out`);
    },

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
    },

    computeAve() {
        const gradeAverage = this.grades.reduce((accumulator, currentValue) => accumulator + currentValue, 0) / 4;
        return gradeAverage;
    },

    willPass() {
        const gradeAverage = this.computeAve();
        return gradeAverage >= 85;
    },

    willPassWithHonors() {
        const gradeAverage = this.computeAve();
        if(gradeAverage >= 90) {
            return true
        } else if(gradeAverage >= 85 && gradeAverage < 90) {
            return false
        } else {
            return undefined
        }
    }
}

let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    
    login() {
        console.log(`${this.email} has logged in`);
    },

    logout() {
        console.log(`${this.email} has logged out`);
    },

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
    },

    computeAve() {
        const gradeAverage = this.grades.reduce((accumulator, currentValue) => accumulator + currentValue, 0) / 4;
        return gradeAverage;
    },

    willPass() {
        const gradeAverage = this.computeAve();
        return gradeAverage >= 85;
    },

    willPassWithHonors() {
        const gradeAverage = this.computeAve();
        if(gradeAverage >= 90) {
            return true
        } else if(gradeAverage >= 85 && gradeAverage < 90) {
            return false
        } else {
            return undefined
        }
    }
}

const classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    honorStudents() {
        return this.students.filter((student) => student.computeAve() >= 90)
    },
    countHonorStudents() {
        return this.honorStudents().length;
    },
    honorsPercentage() {
        return (this.countHonorStudents() / this.students.length) * 100;
    },
    retrieveHonorStudentInfo() {
        return this.honorStudents().map((student) => {
            return({
                email: student.email,
                gradeAverage: student.computeAve()
            })
        })
    },
    sortHonorStudentsByGradeDesc() {
        return this.retrieveHonorStudentInfo().sort((a, b) =>  b.gradeAverage - a.gradeAverage);
    }
}

console.log("1. Translate the other students from our boilerplate code into their own respective objects.");

console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

console.log("2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)");

console.log(studentOne.computeAve());
console.log(studentTwo.computeAve());
console.log(studentThree.computeAve());
console.log(studentFour.computeAve());

console.log("3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail.//For a student to pass, their ave. grade must be greater than or equal to 85.");

console.log(studentOne.willPass());
console.log(studentTwo.willPass());
console.log(studentThree.willPass());
console.log(studentFour.willPass());

console.log("4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, //false if >= 85 but < 90, and undefined if < 85 (since student will not pass).");

console.log(studentOne.willPassWithHonors());
console.log(studentTwo.willPassWithHonors());
console.log(studentThree.willPassWithHonors());
console.log(studentFour.willPassWithHonors());

console.log("5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.");

console.log(classOf1A);

console.log("6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.");

console.log(classOf1A.countHonorStudents());

console.log("7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.");

console.log(classOf1A.honorsPercentage());

console.log("8. Create a method for the object classOf1A named retrieveHonorStudentInfo() //that will return all honor students' emails and ave. grades as an array of objects.");

console.log(classOf1A.retrieveHonorStudentInfo());

console.log("9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.");

console.log(classOf1A.sortHonorStudentsByGradeDesc());