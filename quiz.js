// 1. What is the term given to unorganized code that's very hard to work with?
    // - Spaghetti Code
// 2. How are object literals written in JS?
    // - It is written using the curly brackets ({})
// 3. What do you call the concept of organizing information and functionality to belong to an object?
    // - Object Oriented Programming
// 4. If studentOne has a method named enroll(), how would you invoke it?
    // - studentOne.enroll()
// 5. True or False: Objects can have objects as properties.
    // - True
// 6. What is the syntax in creating key-value pairs?
    // - const myObject = { key: value }
// 7. True or False: A method can have no parameters and still work.
    // - True
// 8. True or False: Arrays can have objects as elements.
    // - True
// 9. True or False: Arrays are objects.
    // - True
// 10. True or False: Objects can have arrays as properties.
    // - True